# Copyright 2018-19 ForgeFlow S.L. (https://www.forgeflow.com)
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl.html).

{
    "name": "MRP Warehouse Calendar",
    "summary": "Considers the warehouse calendars in manufacturing",
    "version": "2.0.1.0.0",
    "license": "LGPL-3",
    "website": "https://gitlab.com/flectra-community/manufacture",
    "author": "ForgeFlow, Odoo Community Association (OCA)",
    "category": "Manufacturing",
    "depends": ["mrp", "stock_warehouse_calendar"],
    "installable": True,
    "development_status": "Production/Stable",
    "maintainers": ["JordiBForgeFlow"],
}
