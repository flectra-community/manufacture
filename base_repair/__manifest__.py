# Copyright 2021 - TODAY, Escflectra
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Base Repair",
    "summary": """
        This module extends the functionality of Odoo Repair module
        to add some basic features.""",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "author": "Escflectra, Agile Business Group, Odoo Community Association (OCA)",
    "maintainers": ["marcelsavegnago"],
    "images": ["static/description/banner.png"],
    "website": "https://gitlab.com/flectra-community/manufacture",
    "category": "Manufacturing",
    "depends": ["repair"],
    "data": ["views/repair_order.xml"],
    "installable": True,
}
