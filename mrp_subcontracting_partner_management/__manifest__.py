{
    "name": "Subcontracting Partner Management",
    "version": "2.0.1.0.0",
    "summary": "Subcontracting Partner Management",
    "author": "Ooops404, Cetmix, Odoo Community Association (OCA)",
    "license": "LGPL-3",
    "category": "Inventory",
    "website": "https://gitlab.com/flectra-community/manufacture",
    "depends": ["purchase_stock", "mrp_subcontracting", "sale_stock"],
    "external_dependencies": {},
    "demo": [],
    "data": [
        "views/res_partner.xml",
    ],
    "qweb": [],
    "installable": True,
    "application": False,
}
