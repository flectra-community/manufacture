# Flectra Community / manufacture

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[mrp_subcontracting_purchase_link](mrp_subcontracting_purchase_link/) | 2.0.1.0.1| Link Purchase Order to Subcontract Productions
[mrp_subcontracting_partner_management](mrp_subcontracting_partner_management/) | 2.0.1.0.0| Subcontracting Partner Management
[mrp_bom_hierarchy](mrp_bom_hierarchy/) | 2.0.1.0.0| Make it easy to navigate through BoM hierarchy.
[base_repair](base_repair/) | 2.0.1.0.0|         This module extends the functionality of Odoo Repair module        to add some basic features.
[mrp_tag](mrp_tag/) | 2.0.1.0.0| Allows to add multiple tags to Manufacturing Orders
[stock_picking_product_kit_helper](stock_picking_product_kit_helper/) | 2.0.1.0.0| Set quanity in picking line based on product kit quantity
[mrp_production_serial_matrix](mrp_production_serial_matrix/) | 2.0.1.1.0| MRP Production Serial Matrix
[mrp_attachment_mgmt](mrp_attachment_mgmt/) | 2.0.1.1.0| Mrp Attachment Mgmt
[repair_refurbish](repair_refurbish/) | 2.0.1.0.1| Create refurbished products during repair
[mrp_account_analytic](mrp_account_analytic/) | 2.0.1.0.1| Consuming raw materials and operations generated Analytic Items
[quality_control_stock_oca](quality_control_stock_oca/) | 2.0.1.0.1| Quality control - Stock (OCA)
[mrp_bom_location](mrp_bom_location/) | 2.0.1.0.1| Adds location field to Bill of Materials and its components.
[mrp_analytic_cost](mrp_analytic_cost/) | 2.0.1.1.0| Track manufacturing costs in real time, using Analytic Items
[repair_discount](repair_discount/) | 2.0.1.0.0| Repair Discount
[mrp_multi_level](mrp_multi_level/) | 2.0.1.6.0| Adds an MRP Scheduler
[mrp_bom_component_menu](mrp_bom_component_menu/) | 2.0.1.0.0| MRP BOM Component Menu
[mrp_production_putaway_strategy](mrp_production_putaway_strategy/) | 2.0.1.0.0| Applies putaway strategies to manufacturing orders for finished products.
[mrp_production_quant_manual_assign](mrp_production_quant_manual_assign/) | 2.0.1.0.0| Production - Manual Quant Assignment
[mrp_warehouse_calendar](mrp_warehouse_calendar/) | 2.0.1.0.0| Considers the warehouse calendars in manufacturing
[mrp_workcenter_hierarchical](mrp_workcenter_hierarchical/) | 2.0.1.0.0| Organise Workcenters by section
[quality_control_oca](quality_control_oca/) | 2.0.1.3.0| Generic infrastructure for quality tests.
[repair_type](repair_type/) | 2.0.1.0.1| Repair type
[mrp_multi_level_estimate](mrp_multi_level_estimate/) | 2.0.1.0.2| Allows to consider demand estimates using MRP multi level.
[account_move_line_mrp_info](account_move_line_mrp_info/) | 2.0.1.0.0| Account Move Line Mrp Info
[mrp_workorder_sequence](mrp_workorder_sequence/) | 2.0.1.0.1| adds sequence to production work orders.
[mrp_bom_tracking](mrp_bom_tracking/) | 2.0.1.1.0| Logs any change to a BoM in the chatter
[repair_stock_move](repair_stock_move/) | 2.0.1.0.1| Ongoing Repair Stock Moves Definition in flectra
[mrp_restrict_lot](mrp_restrict_lot/) | 2.0.1.0.0| MRP Restrict Lot
[mrp_progress_button](mrp_progress_button/) | 2.0.1.1.0| Add a button on MO to make the MO state 'In Progress'
[mrp_planned_order_matrix](mrp_planned_order_matrix/) | 2.0.1.1.0| Allows to create fixed planned orders on a grid view.
[mrp_sale_info](mrp_sale_info/) | 2.0.1.1.0| Adds sale information to Manufacturing models
[mrp_production_grouped_by_product](mrp_production_grouped_by_product/) | 2.0.1.0.2| Production Grouped By Product


