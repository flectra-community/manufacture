# Copyright 2019 ForgeFlow S.L. (https://www.forgeflow.com)
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl).

{
    "name": "MRP BoM Tracking",
    "version": "2.0.1.1.0",
    "author": "ForgeFlow, Odoo Community Association (OCA)",
    "summary": "Logs any change to a BoM in the chatter",
    "website": "https://gitlab.com/flectra-community/manufacture",
    "category": "Manufacturing",
    "depends": ["mrp"],
    "data": ["views/bom_template.xml"],
    "license": "LGPL-3",
    "installable": True,
    "development_status": "Production/Stable",
}
