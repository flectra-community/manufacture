# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "MRP Restrict Lot",
    "version": "2.0.1.0.0",
    "category": "Manufacturing",
    "author": "Akretion,Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/manufacture",
    "license": "AGPL-3",
    "maintainers": ["florian-dacosta"],
    "development_status": "Beta",
    "depends": [
        "stock_restrict_lot",
        "mrp",
    ],
    "installable": True,
}
