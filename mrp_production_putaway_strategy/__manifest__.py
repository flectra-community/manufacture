# Copyright 2017 ForgeFlow S.L.
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

{
    "name": "MRP Production Putaway Strategy",
    "summary": "Applies putaway strategies to manufacturing orders for "
    "finished products.",
    "version": "2.0.1.0.0",
    "author": "ForgeFlow, " "Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/manufacture",
    "category": "Manufacture",
    "depends": ["mrp"],
    "license": "AGPL-3",
    "installable": True,
}
