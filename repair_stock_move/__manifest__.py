# Copyright (C) 2021 ForgeFlow S.L.
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl.html)

{
    "name": "Repair Stock Move",
    "version": "2.0.1.0.1",
    "development_status": "Alpha",
    "license": "LGPL-3",
    "category": "Repair",
    "summary": "Ongoing Repair Stock Moves Definition in flectra",
    "author": "ForgeFlow, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/manufacture",
    "depends": ["repair"],
    "data": [
        "views/repair_order_views.xml",
    ],
    "installable": True,
    "application": False,
}
